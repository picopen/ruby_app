#!/usr/bin/env ruby

require_relative 'lib/base'
require_relative 'lib/log'

# NOTE: can't name the module Parser, because rubocop already uses a gem named so
class MyParser
  class FilePathNotGiven       < StandardError; end
  class FileNotFound           < StandardError; end
  class FileFormatNotSupported < StandardError; end

  FILE_FORMAT_MAPPINGS = {
    ".log" => Log,
  }.freeze

  AVAILABLE_FILE_FORMATS = FILE_FORMAT_MAPPINGS.keys.freeze

  def initialize(file_path)
    raise FilePathNotGiven if file_path.nil?

    @file_path = file_path
    klass      = find_parser_klass
    @parser    = klass.new(file_path)
  end

  def parse
    @parser.output
  end

  private

  def find_parser_klass
    validate_file!
    FILE_FORMAT_MAPPINGS[file_extname]
  end

  def validate_file!
    file_exists?
    format_available?
  end

  def file_exists?
    raise FileNotFound unless File.exists?(@file_path)
  end

  def file_extname
    @file_extname ||= File.extname(@file_path)
  end

  def format_available?
    raise FileFormatNotSupported unless AVAILABLE_FILE_FORMATS.include?(file_extname)
  end
end

unless ENV['RACK_ENV'] == 'test'
  output = MyParser.new(ARGV[0]).parse
  puts output
end
