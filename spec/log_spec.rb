require_relative '../lib/log.rb'

RSpec.describe Log do
  let(:file_path) { "spec/fixtures/test.log" }

  it "accepts a file_path argument" do
    expect(Log).to receive(:new).with(file_path)
    Log.new(file_path)
  end

  describe "#output" do
    it "reads and returns custom output" do
      expect_any_instance_of(Log).to receive(:read_file)
      expect(Log.new(file_path).output).to be_an_instance_of(String)
    end

    context "when ip column is blank" do
      let(:file_path) { "spec/fixtures/error.log" }

      it "raises IPInvalid" do
        expect { Log.new(file_path).output }.to raise_error(Log::IPInvalid)
      end
    end
  end
end
