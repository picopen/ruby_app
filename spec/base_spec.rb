require_relative '../lib/base.rb'

RSpec.describe Base do
  let(:file_path) { "spec/fixtures/test.log" }

  it "accepts a file_path argument" do
    expect(Base).to receive(:new).with(file_path)
    Base.new(file_path)
  end

  context "when #output is called" do
    it "raise MethodNotImplemented" do
      expect { Base.new(file_path).output }.to raise_error(Base::MethodNotImplemented)
    end
  end
end
