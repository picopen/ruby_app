require_relative '../parser.rb'

RSpec.describe MyParser do
  it "accepts a file_path argument and parses it" do
    expect_any_instance_of(Log).to receive(:read_file)
    MyParser.new("spec/fixtures/test.log").parse
  end

  context "when file_path argument is not given" do
    it "raises FilePathNotGiven error" do
      expect { MyParser.new(nil) }.to raise_error(MyParser::FilePathNotGiven)
    end
  end

  context "when file doesn't exist" do
    it "raises FileNotFound error" do
      expect { MyParser.new("some-file_name.log").parse }.to raise_error(MyParser::FileNotFound)
    end

    context "when file format is not supported" do
      it "raises FileFormatNotSupported error" do
        expect_any_instance_of(MyParser).to receive(:file_exists?)
        expect do
          MyParser.new("some-file_name.www").parse
        end.to raise_error(MyParser::FileFormatNotSupported)
      end
    end
  end
end
