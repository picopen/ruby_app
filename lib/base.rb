class Base
  class MethodNotImplemented < StandardError; end

  def initialize(file_path)
    @file_path = file_path
  end

  def output
    raise MethodNotImplemented
  end
end
