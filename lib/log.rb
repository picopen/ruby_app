require "csv"
require "set"
require "ipaddress"
# require "pry"

class Log < Base
  class NullValueError < StandardError; end
  class IPInvalid < StandardError; end

  SEPARATOR = " ".freeze

  def initialize(file_path)
    super(file_path)
    @pages = Hash.new do |hash, key|
      hash[key] = {
        unique_views: Set.new,
        total_count: 0,
      }
    end
  end

  def output
    read_file

    output = most_views_output
    output << "\n\n"
    output << unique_views_output
    output
  end

  private

  def read_file
    CSV.open(@file_path, col_sep: SEPARATOR).lazy.each do |page, ip|
      page_valid?(page)
      ip_valid?(ip)

      @pages[page][:unique_views] << ip
      @pages[page][:total_count] += 1
    end
  end

  def page_valid?(page)
    raise NullValueError if blank?(page)
  end

  def ip_valid?(ip)
    # NOTE: looks like it's not a valid ip
    # raise IPInvalid unless IPAddress.valid?(ip)
    raise IPInvalid if blank?(ip)
  end

  def blank?(value)
    value.to_s.strip.empty?
  end

  def most_views_output
    # TODO: extract to another class maybe
    output = "list of webpages with most page "\
      "views ordered from most pages views to less page views \n\n"
    most_views = build_most_views
    most_views = sort_views(most_views)
    output << most_views.map { |page, count| "#{page} #{count} visits" }.join(" ")
    output
  end

  def unique_views_output
    # TODO: extract to another class maybe
    output = "unique page views also ordered \n\n"
    unique_views = build_unique_views
    unique_views = sort_views(unique_views)
    unique_views.each { |page, count| output << "#{page} #{count} unique views \n" }
    output
  end

  def build_most_views
    @pages.map do |page, hash|
      [page, hash[:total_count]]
    end.lazy
  end

  def build_unique_views
    @pages.map do |page, hash|
      [page, hash[:unique_views].count]
    end.lazy
  end

  def sort_views(views)
    views.sort_by(&:last).reverse
  end
end
