# Parse Ip App

To store the extracted data from the file I used the following code:  
```ruby
Hash.new do |hash, key|
    hash[key] = {
        unique_views: Set.new,
        total_count: 0,
    }
end
```
In the long run (with bigger files) it might not be the best solution, but it looks like it's the shortest and it's readable.  

The number of specs is kinda low, but I was careful to have good coverage. (at least that's what `simplecov` shows)  

The test itself was fun to do, I think I struggled the most with the specs,  
wish I could have used more complex `rspec` logic or the `factory_bot` gem to prove my skills in testing. 
(at least, these ones I use on daily basis in Rails)

## Install  
```bash
  bundle install
```

## Run
the provided `webserver.log` file is commited in repo, so you should only run:

```bash
    bundle exec ./parser.rb webserver.log
```

## Test (using rspec)
```bash
  bundle exec rspec
```

## Coverage
```bash
  COVERAGE=true bundle exec rspec
```

## Linter and code style checker
I have used the `rubocop-airbnb` gem, it's really easy to set and I like the cops settings.  
```bash
  rubocop
```
